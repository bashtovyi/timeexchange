<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Запросы</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="./css/design.css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<%@ include file="header.jsp" %> 
		<p>Ваш запрос был сделан</p>
		<div class="navbar-bottom row-fluid">
      		<div class="navbar-inner">
          		<div class="container">
          			<div class="footer-bg">
						<div class="copyright">
							<p>Copyright &copy; swapcheg.com 2013 - 2014</p>
							<script type="text/javascript">
								document
										.write("<a href=mailto:"+em3+">contact<\/a>");
							</script>
							<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
						</div>
					</div>
				</div>
         	</div>
    	</div>
	</body>
</html>