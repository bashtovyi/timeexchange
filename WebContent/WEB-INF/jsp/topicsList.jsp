<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Доска предложений</title>
		<link rel="stylesheet" href="./css/design.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="./js/scroll.js" type="text/javascript"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
	<%@ include file="header.jsp" %> 
	<div class="container">
		<div class="content">
			<div class="wrapper">
				<div class="proper-content">
					<div class="row">
					<div class="span12">
					<div>
					    </div>
						<c:forEach var="each" items="${topicsList}">
							<div class="panel panel-info" style="margin: 0px 14px; border-width: 0px; padding-top: 10px;">
								<div class="panel-heading">
								<h3 class="panel-title"><a href="topic.action?id=${each.getId()}">${each.getTitle()} </a>${each.getCreationDate()}</h3>
								</div>
								<div class="panel-body media" style="padding: 10px 5px; margin-top: 0px;">
									<a class="pull-left" href="topic.action?id=${each.getId()}">
										<img  src="./images/users/avatars/${each.getUser().getAvatar()}" style="width: 150px; height: 150px" class="thumbnail">
										 ${each.getUser().getFirstName()} ${each.getUser().getSurname()}
									</a>
								<div class="media-body">
							    	${each.getShortDescription()} 
								</div>
								</div>
							</div>
						</c:forEach>
					</div>
					</div>
				</div><!-- /.proper-content -->
				 
			<div class="push"></div>
			</div><!-- /.wrapper -->
		   	<footer>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                    <ul class="list-inline">
	                        <li><a  href="https://ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
	                        </li>
	                        <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
	                        </li>
	                        <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
	                        </li>
	                    </ul>
	                    <hr>
	                    <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
						<script type="text/javascript">
							document
									.write("<a href=mailto:"+em3+">contact<\/a>");
						</script>
						<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
	                </div>
	            </div>
	        </div>
	    </footer>
		</div>
	</div>
    <div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">Наверх</span></div>
	</body>
</html>