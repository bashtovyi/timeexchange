<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Обьявление</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
		<link rel="stylesheet" href="./css/design.css">
		<link rel="stylesheet" href="./css/styles-carousel.css">
	 	<link href="./css/stylish-portfolio.css" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="./js/jquery.CarouselLifeExample.js" type="text/javascript"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(function() {
				$("#sendRequest")
						.click(
								function() {
									var message = $("#message").val();
		
									var request = $
											.ajax({
												url : "create-request-ajax.action?topicId=${topic.getId()}&message="
														+ message,
												type : "GET",
												dataType : "text"
											});
									request
											.done(function(msg) {
												if (msg.indexOf("true", 0) > -1) {
													$("#sendMessageBox")
															.html(
																	"<h4 style=\"color: green;\">Запрос отправлен</h4>");
												} else {
													$("#sendMessageBox")
															.html(
																	"<h4 style=\"color: red;\">Ошибка отправки запроса</h4>");
												}
											});
		
									request
											.fail(function(jqXHR, textStatus) {
												$("#sendMessageBox")
														.html(
																"<h4 style=\"color: red;\">Ошибка отправки запроса</h4>");
											});
		
									$("#sendMessageBox").show();
								});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.container').Carousel({
					visible : 5,
					rotateBy : 1,
					speed : 1000,
					btnNext : '#next',
					btnPrev : '#prev',
					auto : false,
					backslide : true,
					margin : 10
				});
			});
		</script>
	</head>
<body>
	<%@ include file="header.jsp"%>
						<div class="panel panel-default">
										<div class="panel-body">
							<ul class="list-group">
								<li class="list-group-item">Автор: ${topic.getUser().getFirstName()} <br>
									Цена:${topic.getPrice()}</li>
								<li class="list-group-item">${topic.getLongDescription()}</li>
									<li class="list-group-item">
									<c:if test="${!imagesList.isEmpty()}">
									<div class="table-responsive">
										<table class="table table-striped">
											<tr>
												<td>
													<div class="container">
														<ul class="carousel">
															<c:forEach var="each" items="${imagesList}">
																<li><a href="./images/topic_images/${each}" target="blank"><img src="./images/topic_images/${each}"style="width: 200px; height: 200px; display: inline;"class="thumbnail"></a></li>
															</c:forEach>
														</ul>
													
													</div>
													<div id="console"></div>
												</td>
											</tr>
										</table>
									</div>
										<c:if test="${!imagesList.isEmpty()}">
										<div style="width: 50%; margin: 0 auto; text-align: center;">
											<button  id="prev">Назад</button>
											<button id="next">Вперед</button>	
										</div>
										</c:if>
									</c:if>
								</li>
							</ul>
							</div>
							
					<c:if test="${user != null}">
						<c:if test="${user.getId() == topic.getUser().getId()}">
							<form class="form-signin" role="form" method="post"
								action="./uploadTopicImage.action" enctype="multipart/form-data">
								<input type="hidden" name="topicId" value="${topic.getId()}">
										<div style="position: relative;">
											<a class='btn btn-primary form-control' href='javascript:;'>
												Выбрать фото... <input type="file"
												style='position: absolute; z-index: 2; top: 0; left: 0; filter: alpha(opacity = 0); -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; opacity: 0; background-color: transparent; color: transparent;'
												name="file" size="40"
												onchange='$("#upload-file-info").html($(this).val());'>
											</a> <span class='label label-info' id="upload-file-info"></span>
										</div>
								<button type="submit"
									class="form-control btn btn-success btn-block">Загрузить</button>
							</form>
						</c:if>
					</c:if>
							</div>
							<c:if test="${requestsList != null}">
								<div class="table-responsive">
									<table class="table table-striped" >
										<thead>
											<tr >
												<th width="25%">Имя пользователя</th>
												<th width="25%">Сообщение</th>
												<th width="25%">Статус</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="each" items="${requestsList}">
												<tr>
													<td>${each.getOwner().getFirstName()}
														${each.getOwner().getLastName()}
													</td>
													<td>
														${each.getMessage()}
													</td>
													<td>
														<c:if test="${each.getApprove() == 1}"><img src="./icons/like.png"></c:if> 
														<c:if test="${each.getApprove() == 2}"><img src="./icons/dislike.png"></c:if> 
														<c:if test="${each.getApprove() == 0}">
															<a
																href="approve-request.action?topicId=${topic.getId()}&requestId=${each.getId()}&approve=1"><img
																src="./icons/like.png"></a>
															<a
																href="approve-request.action?topicId=${topic.getId()}&requestId=${each.getId()}&approve=2"><img
																src="./icons/dislike.png"></a>
														</c:if>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</c:if>
							<c:if test="${user != null}">
								<c:if test="${user.getId() != topic.getUser().getId()}">
									<div class="panel panel-default">
										<div class="panel-body">
											<textarea rows="2" cols="25" maxlength="100" id="message"
												class="form-control" style="margin-bottom: 10px;" required
												placeholder="Сообщение"></textarea>
											<input id="sendRequest"
												class="btn btn-lg btn-primary btn-block" type="submit"
												value="Отправить запрос" style="width: 170px;">
											<div id="sendMessageBox" style="display: none;"></div>
										</div>
									</div>
								</c:if>
							</c:if>
							
	</body>
</html>