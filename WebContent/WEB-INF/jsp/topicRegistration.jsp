<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Создание заявки</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
		<link rel="stylesheet" href="./css/ui-lightness/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" href="./ui-lightness/jquery-ui-1.10.4.custom.min.css">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="./js/jquery-ui-1.10.4.custom.js"></script>
		<script src="./js/jquery-ui-1.10.4.custom.min.js"type="text/javascript"></script>
		<script src="./js/citiesList.js" type="text/javascript"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<style>
		.ui-autocomplete-loading {
		background: white url('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/images/ui-anim_basic_16x16.gif') right center no-repeat;
		}
		</style>
	</head>
	<body>
<%@ include file="header.jsp" %> 
	<div class="container">
		<div class="content">
			<div class="wrapper">
				<div class="proper-content">
					<div class="row">
						<div class="span12">
							<form class="form-signin" role="form" method="post" id="topic-form">
								<div style="width: 50%; margin: 0 auto; text-align: center;">
									<h4 class="form-signin-heading">Создать   заявку</h4>
								</div>
								<select id="country-id" name="country-id" class="form-control" style="margin-bottom: 10px;" onchange="autoSubmit()"  autofocus required>
								<c:choose>
								<c:when test="${countryId > 0}">
								<c:forEach var="each" items="${countriesList}">
									<c:if test="${each.getId() == countryId}">
										<option id="${each.getId()}" value="${each.getId()}">${each.getName()}</option>
									</c:if>
								</c:forEach>
								</c:when>
								<c:otherwise>
									<option value="0">Страна</option>
								<c:forEach var="each" items="${countriesList}">
									<option id="${each.getId()}" value="${each.getId()}" <c:if test="${each.getId() == countryId}">selected</c:if>>${each.getName()}</option>
								</c:forEach>
								</c:otherwise>
								</c:choose> 
								</select>
								<input type="text" id="city" name="city" class="form-control" style="margin-bottom: 10px;" required>
								<input type="text" id="create-date" name="creation-date" class="form-control" style="margin-bottom: 10px;" value="${creationDate}" required>
								<input type="text" id="expire-date" name="expiry-date" class="form-control" style="margin-bottom: 10px;" value="${expiryDate}"  required>
								<input type="text" name="title" class="form-control" value="${title}" style="margin-bottom: 10px;" required placeholder="Заголовок">
								<textarea rows="4" cols="50" maxlength="200" name="short-description" class="form-control" style="margin-bottom: 10px;" required placeholder="Краткое описание">${shortDescription}</textarea>
								<textarea rows="8" cols="50" name="long-description" class="form-control" style="margin-bottom: 10px;" required placeholder="Полное описание">${longDescription}</textarea>
								<input type="text" name="price" class="form-control" style="margin-bottom: 10px;" placeholder="Цена" value="${price}" pattern="\d+">
								<input class="btn btn-lg btn-primary btn-block" type="submit" value="Создать заявку">
							</form>
								</div>
										</div>
									</div><!-- /.proper-content -->
				 
								<div class="push"></div>
								</div><!-- /.wrapper -->
								<footer>
						        <div class="container">
						            <div class="row">
						                <div class="col-md-6 col-md-offset-3 text-center">
						                    <ul class="list-inline">
						                        <li><a  href="https://ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
						                        </li>
						                        <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
						                        </li>
						                        <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
						                        </li>
						                    </ul>
						                    <hr>
						                    <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
												<script type="text/javascript">
													document
															.write("<a href=mailto:"+em3+">contact<\/a>");
												</script>
												<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
						                </div>
						            </div>
						        </div>
						    </footer>
							</div>
						</div>
						    <div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">Наверх</span></div>
						   	<script>
								$(function($) {
									$("#create-date").datepicker();
								});
								$(function($) {
									$("#expire-date").datepicker();
								});
							</script>
						</body>
					</html>