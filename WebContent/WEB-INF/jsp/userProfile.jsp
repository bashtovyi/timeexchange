<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Мой профиль</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
		<link href="./css/bootstrap.css" rel="stylesheet">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
	    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
	    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<%@ include file="header.jsp"%>
		<div class="container">
		<div class="content">
			<div class="wrapper">
				<div class="proper-content">
					<div class="row">
						<div class="span12">
							<form class="form-signin" role="form" method="post">
								<div style="width: 50%; margin: 0 auto; text-align: center;">
								<img  src="./images/users/avatars/${user.getAvatar()}" style="width: 150px; height: 150px" class="thumbnail">
								</div>
								<input type="text" style="margin-bottom: 10px;" name="firstname" class="form-control"  value="${user.getFirstName()}" autofocus="autofocus" required="required" placeholder="Имя">
								<input type="text" style="margin-bottom: 10px;" name="lastname" class="form-control" value="${user.getLastName()}" required="required" placeholder="Фамилия">
								<input type="password" style="margin-bottom: 10px;" name="password" class="form-control" value="${user.getPassword()}" required="required" placeholder="Пароль">
								<input type="email" style="margin-bottom: 10px;" name="email" class="form-control" value="${user.getEmail()}" required="required" placeholder="E-mail">
								<input type="tel" style="margin-bottom: 20px;" name="telephone" class="form-control" value="${user.getTelephone()}" required="required" placeholder="+380968775771">
								<button type="submit" class="form-control btn btn-success btn-block" >Сохранить</button>
							</form>
							<form class="form-signin" role="form" method="post" action="./changeAvatar.action" enctype="multipart/form-data">
									<div style="position:relative;">
									<a class='btn btn-primary form-control' href='javascript:;'>
										  Выбрать аватар...
									<input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
									</a>
											<button type="submit" class="form-control btn btn-success btn-block">Сменить аватар</button>
									</div>
							</form>
			</div>
					</div>
				</div><!-- /.proper-content -->
				<div class="push"></div>
				</div><!-- /.wrapper -->
					<footer>
				        <div class="container">
				            <div class="row">
				                <div class="col-md-6 col-md-offset-3 text-center">
				                    <ul class="list-inline">
				                       <li><a  href="https://ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
				                       </li>
				                       <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
				                       </li>
				                       <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
				                       </li>
				                    </ul>
		                  		 	 <hr>
				                    <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
									<script type="text/javascript">
															document
																	.write("<a href=mailto:"+em3+">contact<\/a>");
									</script>
									<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
				                </div>
				            </div>
				        </div>
			    </footer>
			</div>
		</div>
	    <div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">Наверх</span></div>
	</body>
</html>