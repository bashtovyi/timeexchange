<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Мои обьявления</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="./css/design.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<%@ include file="header.jsp"%>
		<div class="container">
		<div class="content" style="padding-top: 1px; margin-top: 10px; margin-bottom: 0px; padding-bottom: 0px;">
			<div class="wrapper">
				<div class="proper-content">
					<div class="row">
						<div class="span12">
						<div class="panel panel-default">
							<div class="panel-body">
								<ul class="list-group">
									<c:forEach var="each" items="${topicsList}">
										<li class="list-group-item">Тема: <a href="topic.action?id=${each.getId()}">${each.getTitle()}</a></li>
										<li class="list-group-item">Описание: ${each.getShortDescription()} </li>
									</c:forEach>
					    		</ul>
				    		</div>
						</div>
				</div>
			</div>
		
			</div>
				</div><!-- /.proper-content -->
			<div class="push"></div>
			<footer>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                    <ul class="list-inline">
	                        <li><a  href="ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
	                        </li>
	                        <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
	                        </li>
	                        <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
	                        </li>
	                    </ul>
	                    <hr>
	                    <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
							<script type="text/javascript">
								document
										.write("<a href=mailto:"+em3+">contact<\/a>");
							</script>
							<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
	                </div>
	            </div>
	        </div>
	    </footer>
			</div><!-- /.wrapper -->
			</div>
	</body>
</html>