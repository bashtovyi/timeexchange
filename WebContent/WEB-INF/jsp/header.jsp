<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		 <div class="container-fluid">
		   <!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			 <a class="navbar-brand" href="./">Swapcheg</a>
			</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <c:if test="${user == null}">
				<form action="./registration.action" method="get" class="navbar-form navbar-right" role="form">
					<button type="submit" class="btn btn-success">Регистрация</button>
				</form>
				<form action="./login.action" method="post" class="navbar-form navbar-right" role="form">
					<div class="form-group">
						<input type="text" name="login" placeholder="Email" class="form-control">
					</div>
					<div class="form-group">
						<input type="password" name="password" placeholder="Пароль" class="form-control">
					</div>
					<button type="submit" class="btn btn-success">Войти</button>
				</form>
			</c:if>
			<ul class="nav navbar-nav navbar-left">
			  <li class="active"><a href="./topicsList.action">Обьявления</a></li>
			  <c:if test="${user != null}">
			</c:if>
				<c:if test="${user != null}">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Больше<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="./myTopics.action">Мои обьявления</a></li>
						<li><a href="./myRequests.action">Запросы</a></li>
						<li><a href="./topicRegistration.action">Cоздать топик</a></li>
						<li><a href="./profile.action">Мой профиль</a></li>
					</ul>
				</li>
				</c:if>
			</ul>
			<c:if test="${user != null}">
				<form action="./login.action" class="navbar-form navbar-right">
					<input type="hidden" name="logout">
					<button type="submit" class="btn btn-success">Выход [${user.getEmail()}]</button>
				</form>
			</c:if>
				<form class="navbar-form navbar-left" action="topicsList.action">
					<input type="text" class="form-control" placeholder="Поиск..." name="search">
				</form>
			 </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
				
					