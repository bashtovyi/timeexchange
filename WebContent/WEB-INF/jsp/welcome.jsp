<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	    <title>Swapcheg.com</title>
	    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
	    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
	    <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle"><i class="fa fa-bars"></i></a>
	    <div id="sidebar-wrapper">
	        <ul class="sidebar-nav">
	            <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
	            <li class="sidebar-brand"><a href="swapcheg.com">Меню</a>
	            </li>
	            <li><a href="#top">Домой</a>
	            </li>
	            <li><a href="#about">О чем Мы?</a>
	            </li>
	            <li><a href="#services">Возможности</a>
	            <li><a href="#contact">Контакты</a>
	            </li>
	        </ul>
	    </div>
	    <!-- /Side Menu -->
	    <!-- Full Page Image Header Area -->
	    <div id="top" class="header">
	        <div class="vert-text">
	            <h1>Swapcheg</h1>
	            <h3>
	                <em>Мы</em> Поможем,
	                <em>Вам</em> Найти </h3>
	            <a href="./topicsList.action" class="btn btn-default btn-lg">Загляни</a>
	        </div>
	    </div>
	    <!-- /Full Page Image Header Area -->
	
	    <!-- Intro -->
	    <div id="about" class="intro">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                    <h2>Привет. Этот сайт создан помочь Вам найти и разместить Ваши обьявления!</h2>
	                    <p class="lead">Вы вольны размещать все что Вам по душе!<br><a target="_blank" href="./registration.action">Зарегистрироваться</a></p>
	                    <p class="lead"><a target="_blank" href="./topicsList.action">Войти</a></p>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- /Intro -->
	    <!-- Services -->
	    <div id="services" class="services">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-4 col-md-offset-4 text-center">
	                    <h2>Возможности</h2>
	                    <hr>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-2 col-md-offset-2 text-center">
	                    <div class="service-item">
	                        <i class="service-icon fa fa-rocket"></i>
	                        <h4>Выставить обьявление</h4>
	                    </div>
	                </div>
	                <div class="col-md-2 text-center">
	                    <div class="service-item">
	                        <i class="service-icon fa fa-magnet"></i>
	                        <h4>Найти</h4>
	                    </div>
	                </div>
	                <div class="col-md-2 text-center">
	                    <div class="service-item">
	                        <i class="service-icon fa fa-shield"></i>
	                        <h4>Галерея</h4>
	                    </div>
	                </div>
	                <div class="col-md-2 text-center">
	                    <div class="service-item">
	                        <i class="service-icon fa fa-pencil"></i>
	                        <h4>Фидбэк</h4>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- /Services -->
	    <!-- Footer -->
	    <div id="contact">
	    <footer>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                    <ul class="list-inline">
	                        <li><a  href="https://ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
	                        </li>
	                        <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
	                        </li>
	                        <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
	                        </li>
	                    </ul>
	                    <div class="top-scroll">
	                        <a href="#top"><i class="fa fa-circle-arrow-up scroll fa-4x"></i></a>
	                    </div>
	                    <hr>
	                   <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
							<script type="text/javascript">
								document
										.write("<a href=mailto:"+em3+">contact<\/a>");
							</script>
							<p><a href="mailto:bashtovyi@gmail.com">contact</a></p>
	                </div>
	            </div>
	        </div>
	    </footer>
	    </div>
	    <!-- /Footer -->
	    <!-- JavaScript -->
	    <script src="js/jquery-1.10.2.js"></script>
	    <script src="js/bootstrap.js"></script>
	    <!-- Custom JavaScript for the Side Menu and Smooth Scrolling -->
	    <script>
	    $("#menu-close").click(function(e) {
	        e.preventDefault();
	        $("#sidebar-wrapper").toggleClass("active");
	    });
	    </script>
	    <script>
	    $("#menu-toggle").click(function(e) {
	        e.preventDefault();
	        $("#sidebar-wrapper").toggleClass("active");
	    });
	    </script>
	    <script>
	    $(function() {
	        $('a[href*=#]:not([href=#])').click(function() {
	            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
	
	                var target = $(this.hash);
	                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	                if (target.length) {
	                    $('html,body').animate({
	                        scrollTop: target.offset().top
	                    }, 1000);
	                    return false;
	                }
	            }
	        });
	    });
	    </script>
	</body>
</html>