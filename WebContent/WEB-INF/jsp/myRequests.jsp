<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Запросница</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="./css/design.css">
	    <link href="./css/stylish-portfolio.css" rel="stylesheet">
	    <link href="./font-awesome/css/font-awesome.min.css" rel="stylesheet">
	   	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<%@ include file="header.jsp"%>
		<div class="container">
		<div class="content" style="padding-top: 1px; margin-top: 10px; margin-bottom: 0px; padding-bottom: 0px;">
			<div class="wrapper">
				<div class="proper-content">
					<div class="row">
						<div class="span12">
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th width=10%>Имя пользователя</th>
						<th width=20%>Сообщение</th>
						<th width=10%>Статус</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="each" items="${requestsList}">
						<tr>
							<td>${each.getOwner().getFirstName()} ${each.getOwner().getLastName()}</td>
							<td>${each.getMessage()}</td>
							<td>
								<c:if test="${each.getApprove() == 1}">
									<img  src="./icons/like.png">
								</c:if>
								<c:if test="${each.getApprove() == 2}">
									<img  src="./icons/dislike.png">
								</c:if>
								<c:if test="${each.getApprove() == 0}">
									<img  src="./icons/refresh.png">
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		</div>
					</div>
				</div><!-- /.proper-content -->
			<div class="push"></div>
			</div><!-- /.wrapper -->
		</div>
		<footer>
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                    <ul class="list-inline">
	                        <li><a  href="https://ua.linkedin.com/in/obashtovoj" target="blank"><i class="fa fa-linkedin fa-3x"></i></a>
	                        </li>
	                        <li><a href="https://www.facebook.com/tox1fer" target="blank"><i class="fa fa-facebook fa-3x"></i></a>
	                        </li>
	                        <li><a  href="https://twitter.com/Obashtovyi" target="blank"><i class="fa fa-twitter fa-3x"></i></a>
	                        </li>
	                    </ul>
	                    <hr>
	                    <p>Copyright &copy; swapcheg.com 2013 - 2014</p>
	                </div>
	            </div>
	        </div>
	    </footer>
	</div>
	</body>
</html>