$(function() {
	$("#city").autocomplete({
		source : function(request, response) {
			$.get("citiesList.action", {
				term : request.term,
				countryId : $("#country-id").children(":selected").attr("id")
			}, function(data) {
				var data = jQuery.parseJSON(data);
				response(data);
			});
		},
		minLength : 3
	});
});