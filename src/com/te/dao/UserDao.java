package com.te.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.te.beans.City;
import com.te.beans.Country;
import com.te.beans.Region;
import com.te.beans.User;

@Repository
@SuppressWarnings("unchecked")
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public boolean isRegistered(String login, String password) {
        List<User> usersList = sessionFactory.getCurrentSession().createQuery("from User where email =  '" + login + "' and password = '" + password + "'")
                .list();

        if (usersList == null) {
            return false;
        }
        if (usersList.isEmpty()) {
            return false;
        }

        return true;
    }

    public void setUserHash(String hash, String email) {
        sessionFactory.getCurrentSession().createQuery("update User set hash = '" + hash + "' where email = '" + email + "'").executeUpdate();
    }

    public User getUserByHash(String hash) {
        User user = (User) sessionFactory.getCurrentSession().createQuery("from User where hash = '" + hash + "'").uniqueResult();

        return user;
    }

    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    public boolean isEmailExists(String email) {
        List<User> usersList = sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).list();
        if (usersList.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean isTelephoneExists(String telephone) {
        List<User> usersList = sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("telephone", telephone)).list();
        if (usersList.isEmpty()) {
            return false;
        }
        return true;
    }

    public List<City> getCitiesList(int regionId) {
        List<City> citiesList = sessionFactory.getCurrentSession().createQuery("from City where regionId = " + regionId).list();
        return citiesList;
    }

    public List<Country> getCountriesList() {
        List<Country> countriesList = sessionFactory.getCurrentSession().createCriteria(Country.class).list();
        return countriesList;
    }

    public List<Region> getRegionsList(int countryId) {
        List<Region> regionsList = sessionFactory.getCurrentSession().createQuery("from Region where countryId = " + countryId).list();
        return regionsList;
    }

    public void changeProfile(String firstName, String lastName, String password, String email, String telephone, int userId) {
        sessionFactory
                .getCurrentSession()
                .createQuery(
                        "update User set firstname = '" + firstName + "', lastname = '" + lastName + "', password = '" + password + "', email = '" + email
                                + "', telephone = '" + telephone + "' where id = " + userId).executeUpdate();
    }

    public void saveAvatarFileName(int userId, String avatarName) {
        sessionFactory.getCurrentSession().createQuery("update User set avatar = '" + avatarName + "' where id = " + userId).executeUpdate();
    }

    public int getRegionId(String region, int countryId) {
        int regionId = (int) sessionFactory.getCurrentSession().createQuery("select id from Region where name = '" + region + "' and countryId = " + countryId).uniqueResult();
        return regionId;
    }

    public int getCityId(String city, int countryId, int regionId) {
        int cityId = (int) sessionFactory.getCurrentSession().createQuery("select id from City where name = '" + city + "' and countryId = " + countryId + " and regionId = " + regionId).uniqueResult();
        return cityId;
    }

}