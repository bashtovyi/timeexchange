package com.te.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.te.beans.Request;
import com.te.beans.User;

@Repository
@SuppressWarnings("unchecked")
public class RequestDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void addRequest(int ownerId, int topicId, String message, Date currentDate) {
        Request request = new Request();
        User owner = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("id", ownerId)).uniqueResult();
        request.setOwner(owner);
        request.setTopicId(topicId);
        request.setMessage(message);
        request.setDate(currentDate.toString());
        sessionFactory.getCurrentSession().save(request);
    }

    public List<Request> getRequestsListByUserId(int ownerId) {
        List<Request> requestsList = sessionFactory.getCurrentSession().createQuery("from Request where ownerId = " + ownerId).list();
        return requestsList;
    }

    public void setApprove(int requestId, int approve) {
        sessionFactory.getCurrentSession().createQuery("update Request set approve = " + approve + " where id= " + requestId + "").executeUpdate();
    }

    public List<Request> getRequestsListByTopicId(int topicId) {
        List<Request> requestsList = sessionFactory.getCurrentSession().createCriteria(Request.class).add(Restrictions.eq("topicId", topicId)).list();
        return requestsList;
    }

}
