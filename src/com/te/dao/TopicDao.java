package com.te.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.te.beans.City;
import com.te.beans.Topic;
import com.te.beans.TopicImage;

@Repository
@SuppressWarnings("unchecked")
public class TopicDao {
    @Autowired
    private SessionFactory sessionFactory;

    public void saveTopic(Topic topic) {
        sessionFactory.getCurrentSession().save(topic);
    }

    public Topic getTopic(int id) {
        Topic topic = (Topic) sessionFactory.getCurrentSession().createCriteria(Topic.class).add(Restrictions.eq("id", id)).uniqueResult();

        return topic;
    }

    public List<Topic> getTopicsList() {
       // List<Topic> topicsList = sessionFactory.getCurrentSession().createCriteria(Topic.class).list();
        List<Topic> topicsList = sessionFactory.getCurrentSession().createQuery("from Topic ORDER BY creationDate DESC ").list();

        return topicsList;
    }

    public List<Topic> getMyTopicsList(int userId) {
        List<Topic> topicsList = sessionFactory.getCurrentSession().createQuery("from Topic where userId = " + userId).list();
        return topicsList;
    }

    public List<Topic> getTopicsListByTitle(String search) {
        List<Topic> topicsList = sessionFactory.getCurrentSession().createQuery("from Topic where title like '%" + search + "%'").list();
        return topicsList;
    }

    public void saveTopicImage(TopicImage topicImage) {
        sessionFactory.getCurrentSession().save(topicImage);
    }

    public List<String> getImagesList(int topicId) {
        List<String> imagesList = sessionFactory.getCurrentSession().createQuery("select imageName from TopicImage where topicId = " + topicId).list();

        return imagesList;
    }

    public List<City> getCitiestList(String term, int countryId) {
        List<City> citiesList =  sessionFactory.getCurrentSession().createQuery("from City where name like '" + term + "%' and country= " + countryId ).list();
        
        return citiesList;
    }

}
