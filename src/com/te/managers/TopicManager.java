package com.te.managers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.te.beans.City;
import com.te.beans.Topic;
import com.te.beans.TopicImage;
import com.te.dao.TopicDao;

@Service
public class TopicManager {

    @Autowired
    private TopicDao topicDao;

    @Transactional
    public void addTopic(Topic topic) {
        topicDao.saveTopic(topic);
    }

    @Transactional
    public Topic getTopic(int id) {
        Topic topic = topicDao.getTopic(id);
        return topic;
    }

    @Transactional
    public List<Topic> getTopicsList() {
        List<Topic> topicsList = topicDao.getTopicsList();
        return topicsList;
    }

    @Transactional
    public List<Topic> getMyTopicsList(int userId) {
        List<Topic> myTopicList = topicDao.getMyTopicsList(userId);
        return myTopicList;
    }

    @Transactional
    public List<Topic> getTopicsListByTitle(String search) {
        List<Topic> topicsTitle = topicDao.getTopicsListByTitle(search);
        return topicsTitle;
    }

    @Transactional
    public boolean addTopicImage(int topicId, MultipartFile file) {
        if (!file.isEmpty()) {
            String legalExtension = ".jpg";
            String originalFilename = file.getOriginalFilename();
            int index = originalFilename.indexOf(legalExtension, originalFilename.length() - 4);
            if (index < 0 || file.getSize() > 10000000) {
                System.out.println("������");
                return false;
                
            }
            String imageName = UUID.randomUUID().toString() + ".jpg";
            try {
                file.transferTo(new File("/var/lib/tomcat7/webapps/ROOT/images/topic_images/" + imageName));
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            TopicImage topicImage = new TopicImage();
            topicImage.setTopicId(topicId);
            topicImage.setImageName(imageName);

            topicDao.saveTopicImage(topicImage);
            return true;
        }

        return false;

    }

    @Transactional
    public List<String> getImagesList(int topicId) {
        List<String> imagesList = topicDao.getImagesList(topicId);
        return imagesList;
    }

    @Transactional
    public String getCitiesListJson(String term, int countryId) {
        List<City> citiesList = topicDao.getCitiestList(term, countryId);
        StringBuilder json = new StringBuilder();

        if (!citiesList.isEmpty()) {
            json.append("[");
            for (int i = 0; i < citiesList.size() - 1; i++) {
                City city = citiesList.get(i);
                json.append("{\"id\":\"" + city.getId() + "\", \"label\": \"" + city.getName() + " - " + city.getRegion().getName() + "\", \"value\": \""
                        + city.getName() + " - " + city.getRegion().getName() + "\"}, ");
            }
            City city = citiesList.get(citiesList.size() - 1);
            json.append("{\"id\":\"" + city.getId() + "\", \"label\": \"" + city.getName() + " - " + city.getRegion().getName() + "\", \"value\": \""
                    + city.getName() + " - " + city.getRegion().getName() + "\"}]");
        } else {
            json.append("[]");
        }
        return json.toString();
    }
}
