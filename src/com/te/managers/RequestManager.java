package com.te.managers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.te.beans.Request;
import com.te.dao.RequestDao;

@Service
public class RequestManager {

	@Autowired
	private RequestDao requestDao;

	@Transactional
	public void addRequest(int userId, int topicId, String message,
			Date currentDate) {
		requestDao.addRequest(userId, topicId, message, currentDate);
	}

	@Transactional
	public List<Request> getMyRequestsList(int ownerId) {
		List<Request> requestsList = requestDao
				.getRequestsListByUserId(ownerId);
		return requestsList;
	}

	@Transactional
	public List<Request> getRequestsListByTopicId(int topicId) {
		List<Request> requestList = requestDao
				.getRequestsListByTopicId(topicId);
		return requestList;
	}

	@Transactional
	public void setApprove(int requestId, int approve) {
		requestDao.setApprove(requestId, approve);
	}

}
