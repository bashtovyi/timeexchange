package com.te.managers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.te.beans.City;
import com.te.beans.Country;
import com.te.beans.Region;
import com.te.beans.User;
import com.te.dao.UserDao;

@Service
public class UserManager {

    @Autowired
    private UserDao userDao;

    @Transactional
    public Cookie getAuthorizationCookie(String email, String password) {
        boolean isRegistered = userDao.isRegistered(email, password);

        if (isRegistered) {
            String uuid = UUID.randomUUID().toString();
            userDao.setUserHash(uuid, email);

            Cookie cookie = new Cookie("bb_data", uuid);
            cookie.setMaxAge(60 * 60 * 24 * 7);

            return cookie;
        }

        return null;
    }

    @Transactional
    public Cookie getDeAutorizationCookie(Cookie[] cookie) {

        for (Cookie each : cookie) {
            if (each.getName().equals("bb_data")) {
                each.setMaxAge(0);
                return each;
            }
        }

        return null;
    }

    @Transactional
    public User getUser(Cookie[] cookiesArray) {
        if (cookiesArray == null) {
            return null;
        }
        String hash = null;
        for (Cookie each : cookiesArray) {
            if (each.getName().equals("bb_data")) {
                hash = each.getValue();
                break;
            }
        }

        User user = null;
        if (hash != null) {
            user = userDao.getUserByHash(hash);
        }

        return user;
    }

    @Transactional
    public User getUser(String hash) {
        User user = null;
        if (hash != null) {
            user = userDao.getUserByHash(hash);
        }

        return user;
    }

    @Transactional
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Transactional
    public boolean isUserExist(String email, String telephone) {
        boolean isEmailExists = userDao.isEmailExists(email);
        boolean isTelephoneExists = userDao.isTelephoneExists(telephone);
        if (isEmailExists || isTelephoneExists) {
            return true;
        }
        return false;
    }

    @Transactional
    public List<Country> getCountriesList() {
        List<Country> countriesList = userDao.getCountriesList();
        return countriesList;
    }

    @Transactional
    public List<City> getCitiesList(int regionId) {
        List<City> citiesList = userDao.getCitiesList(regionId);
        return citiesList;
    }

    @Transactional
    public List<Region> getRegionsList(int countryId) {
        List<Region> region = userDao.getRegionsList(countryId);
        return region;
    }

    @Transactional
    public void changeProfile(String firstName, String lastName, String password, String email, String telephone, int userId) {
        userDao.changeProfile(firstName, lastName, password, email, telephone, userId);

    }

    @Transactional
    public boolean changeAvatar(int userId, MultipartFile file) {
        if (!file.isEmpty()) {
            String legalExtension = ".jpg";
            String originalFilename = file.getOriginalFilename();
            int index = originalFilename.indexOf(legalExtension, originalFilename.length() - 4);
            if (index < 0 || file.getSize() > 100000) {
                return false;
            }
            String avatarName = UUID.randomUUID().toString() + ".jpg";
            try {
                file.transferTo(new File("/var/lib/tomcat7/webapps/ROOT/images/users/avatars/" + avatarName));
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            userDao.saveAvatarFileName(userId, avatarName);
            return true;
        }

        return false;
    }

    @Transactional
    public int getRegionId(String region, int countryId) {
        int regionId = userDao.getRegionId(region, countryId);
        return regionId;
    }

    @Transactional
    public int getCityId(String city,int countryId, int regionId) {
        int cityId = userDao.getCityId(city, countryId, regionId);
        return cityId;
    }
    
}
