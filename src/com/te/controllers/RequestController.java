package com.te.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.Request;
import com.te.beans.User;
import com.te.managers.RequestManager;
import com.te.managers.UserManager;

@Controller
public class RequestController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private RequestManager requestManager;

    @RequestMapping(value = "/create-request.action", method = RequestMethod.GET)
    public ModelAndView getCreateRequest(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("request");
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        return mav;
    }

    @RequestMapping(value = "/create-request-ajax.action", method = RequestMethod.GET)
    public void doPost(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = true) String hash,
            @RequestParam(value = "topicId", required = true) int topicId,
            @RequestParam(value = "message", required = true) String message) throws IOException {
        
        User user = userManager.getUser(hash);

        int userId = user.getId();
        Date currentDate = new Date();
        requestManager.addRequest(userId, topicId, message, currentDate);

        response.getWriter().print("true");
    }

    @RequestMapping(value = "/myRequests.action", method = RequestMethod.GET)
    public ModelAndView getMyRequests(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("myRequests");
        User user = userManager.getUser(hash);
        if (user != null) {
            List<Request> MyRequestsList = requestManager
                    .getMyRequestsList(user.getId());
            mav.addObject("requestsList", MyRequestsList);
        }
        mav.addObject("user", user);

        return mav;
    }

    @RequestMapping(value = "/approve-request.action", method = RequestMethod.GET)
    public ModelAndView getApproveRequest(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = false) String hash,
            @RequestParam(value = "topicId", required = true) int topicId,
            @RequestParam(value = "requestId", required = true) int requestId,
            @RequestParam(value = "approve", required = false) int approve) {
        ModelAndView mav = new ModelAndView();
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        requestManager.setApprove(requestId, approve);

        mav.addObject("user", user);
        return new ModelAndView("redirect:topic.action?id=" + topicId);
    }

}
