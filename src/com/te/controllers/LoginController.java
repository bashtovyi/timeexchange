package com.te.controllers;

import javax.servlet.http.Cookie; 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.User;
import com.te.managers.UserManager;

@Controller
public class LoginController {

	@Autowired
	private UserManager userManager;

	@RequestMapping(value = "/login.action", method = RequestMethod.GET)
	public ModelAndView getLogin(HttpServletRequest request,
			HttpServletResponse response,
			@CookieValue(value = "bb_data", required = false) String hash,
			@RequestParam(value = "logout", required = false) String logout) {
		User user = userManager.getUser(hash);

		if (user != null && logout != null) {
			Cookie deAuthorizationCookie = userManager
					.getDeAutorizationCookie(request.getCookies());
			response.addCookie(deAuthorizationCookie);
		}

		return new ModelAndView("redirect:topicsList.action");
	}
	@RequestMapping(value="/welcome.action", method = RequestMethod.GET)
	public ModelAndView getStart(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = false) String hash) {
	    ModelAndView mav = new ModelAndView("welcome");
	    return mav;
	}
	
        @RequestMapping(value = "/login.action", method = RequestMethod.POST)
	public ModelAndView postLogin(HttpServletResponse response,
			@RequestParam(value = "login", required = true) String login,
			@RequestParam(value = "password", required = true) String password) {
		Cookie authorizationCookie = userManager.getAuthorizationCookie(login,
				password);
		if (authorizationCookie == null) {
			return new ModelAndView("redirect:topicsList.action");
		}
		response.addCookie(authorizationCookie);

		return new ModelAndView("redirect:topicsList.action");
	}
	

}