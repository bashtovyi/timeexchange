package com.te.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.User;
import com.te.managers.TopicManager;
import com.te.managers.UserManager;

@Controller
public class UploadController {
    @Autowired
    private UserManager userManager;

    @Autowired
    private TopicManager topicManager;

    @RequestMapping(value = "/changeAvatar.action", method = RequestMethod.POST)
    public ModelAndView getProfilePost(@RequestParam(value = "file") MultipartFile file, @CookieValue(value = "bb_data", required = false) String hash) {

        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        userManager.changeAvatar(user.getId(), file);

        return new ModelAndView("redirect:profile.action");
    }

    @RequestMapping(value = "/uploadTopicImage.action", method = RequestMethod.POST)
    public ModelAndView uploadTopicImage(@RequestParam(value = "file") MultipartFile file, @RequestParam(value = "topicId", required = true) int topicId,
            @CookieValue(value = "bb_data", required = false) String hash) {

        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        topicManager.addTopicImage(topicId, file);

        return new ModelAndView("redirect:topic.action?id=" + topicId);

    }

}