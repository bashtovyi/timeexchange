package com.te.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.City;
import com.te.beans.Country;
import com.te.beans.Region;
import com.te.beans.Request;
import com.te.beans.Topic;
import com.te.beans.User;
import com.te.managers.RequestManager;
import com.te.managers.TopicManager;
import com.te.managers.UserManager;

@Controller
public class TopicController {

    @Autowired
    private UserManager userManager;
    @Autowired
    private TopicManager topicManager;
    @Autowired
    private RequestManager requestManager;
    @Autowired
    private TopicManager TopicManager;

    @RequestMapping(value = "/topic.action", method = RequestMethod.GET)
    public ModelAndView getTopic(HttpServletResponse response, @CookieValue(value = "bb_data", required = false) String hash,
            @RequestParam(value = "id", required = true) int topicId, @RequestParam(value = "isRequestSend", required = false) boolean isRequestSend) {
        ModelAndView mav = new ModelAndView("topic");
        User user = userManager.getUser(hash);

        Topic topic = topicManager.getTopic(topicId);
        mav.addObject("topic", topic);

        if (user != null) {
            if (user.getId() == topic.getUser().getId()) {
                List<Request> requestsList = requestManager.getRequestsListByTopicId(topicId);
                mav.addObject("requestsList", requestsList);
            }
        }

        if (isRequestSend) {
            mav.addObject("isRequestSend", true);
        }
        mav.addObject("user", user);

        List<String> imagesList = topicManager.getImagesList(topicId);

        mav.addObject("imagesList", imagesList);

        return mav;
    }

    @RequestMapping(value = "/topicRegistration.action", method = RequestMethod.GET)
    public ModelAndView getTopicRegistration(HttpServletResponse response, 
            @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("topicRegistration");
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        List<Country> countriesList = userManager.getCountriesList();
        mav.addObject("countriesList", countriesList);
        mav.addObject("user", user);

        return mav;
    }

    @RequestMapping(value = "/topicRegistration.action", method = RequestMethod.POST)
    public ModelAndView postTopicRegistration(HttpServletResponse response,
            @CookieValue(value = "bb_data", required = true) String hash,
            @RequestParam(value = "creation-date", required = true) String creationDate,
            @RequestParam(value = "expiry-date", required = true) String expiryDate, 
            @RequestParam(value = "title", required = true) String title,
            @RequestParam(value = "short-description", required = true) String shortDescription,
            @RequestParam(value = "long-description", required = true) String longDescription, 
            @RequestParam(value = "price", required = false) Integer price,
            @RequestParam(value = "country-id", required = true) int countryId, 
            @RequestParam(value = "city", required = false) String city) {
        ModelAndView mav = new ModelAndView("topicRegistration");
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        List<Country> countriesList = userManager.getCountriesList();
        mav.addObject("countriesList", countriesList);
        mav.addObject("creationDate", creationDate);
        mav.addObject("expiryDate", expiryDate);
        mav.addObject("title", title);
        mav.addObject("shortDescription", shortDescription);
        mav.addObject("longDescription", longDescription);
        mav.addObject("price", price);
        mav.addObject("user", user);
        mav.addObject("city", city);
        mav.addObject("countryId", countryId);

        if (countryId == 0) {
            return mav;
        }

        List<Region> regionsList = userManager.getRegionsList(countryId);
        mav.addObject("regionsList", regionsList);
        
        
        String[] cityArray = city.split(" - ");
        city = cityArray[0];
        String region = cityArray[1];
        
        int regionId = userManager.getRegionId(region, countryId);
        int cityId = userManager.getCityId(city, countryId, regionId);
        if (regionId == 0) {
            return mav;
        }
        mav.addObject("regionId", regionId);
        
        List<City> citiesList = userManager.getCitiesList(regionId);
        mav.addObject("citiesList", citiesList);

        if (regionId != 0 && cityId != 0) {
            Topic topic = new Topic();

            topic.setCreationDate(creationDate);
            topic.setExpiryDate(expiryDate);
            topic.setTitle(title);
            topic.setShortDescription(shortDescription);
            topic.setLongDescription(longDescription);
            topic.setPrice(price);
            topic.setCountryId(countryId);
            topic.setRegionId(regionId);
            topic.setCityId(cityId);
            topic.setUser(user);

            topicManager.addTopic(topic);
            return new ModelAndView("redirect:topicsList.action");
        }

        return mav;
    }

    @RequestMapping(value = "/topicsList.action", method = RequestMethod.GET)
    public ModelAndView getIndex(HttpServletResponse response, @CookieValue(value = "bb_data", required = false) String hash,
            @RequestParam(value = "search", required = false) String search) {
        ModelAndView mav = new ModelAndView("topicsList");
        User user = userManager.getUser(hash);

        if (user != null) {
            mav.addObject("isAuthorized", true);
            mav.addObject("user", user);
        }

        if (search == null) {
            List<Topic> topicsList = TopicManager.getTopicsList();
            mav.addObject("topicsList", topicsList);
        } else {
            List<Topic> topicsList = TopicManager.getTopicsListByTitle(search);
            mav.addObject("topicsList", topicsList);
        }

        return mav;
    }

    @RequestMapping(value = "/myTopics.action", method = RequestMethod.GET)
    public ModelAndView getMyTopics(HttpServletResponse response, @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("myTopicsList");
        User user = userManager.getUser(hash);

        if (user != null) {
            mav.addObject("isAuthorized", true);
         // �������� ���������� � ������ user �� jsp ��������
            mav.addObject("user", user);
            int userId = user.getId();
            List<Topic> topicsList = TopicManager.getMyTopicsList(userId);
            mav.addObject("topicsList", topicsList);
        }

        mav.addObject("user", user);

         /*�������������� jsp ������� � ������ html ��� (��� jsp, jstl �������).
         �������� request � ����������� � ������� �������
         �������� �������� html ���� ������ �������*/
        return mav;
    }

    @RequestMapping(value = "/citiesList.action", method = RequestMethod.GET)
    public void citiesList(HttpServletResponse response, 
            @RequestParam(value = "term", required = true) String term,
            @RequestParam(value="countryId", required = true) int countryId) throws IOException {
        String json = topicManager.getCitiesListJson(term, countryId);
        response.getWriter().write(json);
    }

}
