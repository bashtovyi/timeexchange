package com.te.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.Country;
import com.te.beans.User;
import com.te.managers.UserManager;

@Controller
public class UserController {

    @Autowired
    private UserManager userManager;

    @RequestMapping(value = "/registration.action", method = RequestMethod.GET)
    public ModelAndView getRegistration(HttpServletRequest request, @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("registration");
        User user = userManager.getUser(hash);

        if (user == null) {
            List<Country> countriesList = userManager.getCountriesList();
            request.setAttribute("countriesList", countriesList);
            return mav;
        }

        return new ModelAndView("redirect:topicsList.action");
    }

    @RequestMapping(value = "/registration.action", method = RequestMethod.POST)
    public ModelAndView postLogin(@CookieValue(value = "bb_data", required = false) String hash,
            @RequestParam(value = "firstname", required = true) String firstName, 
            @RequestParam(value = "lastname", required = true) String lastName,
            @RequestParam(value = "password", required = true) String password, 
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "telephone", required = true) String telephone, 
            @RequestParam(value = "country-id", required = true) int countryId) {
        ModelAndView mav = new ModelAndView("registration");
        User user = userManager.getUser(hash);

        if (user == null) {
            mav.addObject("isUserNotCreated", true);

            boolean isUserExist = userManager.isUserExist(email, telephone);
            if (isUserExist) {
                mav.addObject("isUserNotCreated", true);
                List<Country> countriesList = userManager.getCountriesList();
                mav.addObject("countriesList", countriesList);
                return new ModelAndView("redirect:registration.action");
            } else {
                user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setPassword(password);
                user.setEmail(email);
                user.setTelephone(telephone);
                user.setCountryId(countryId);
                user.setAvatar("default.jpg");
                userManager.addUser(user);

                mav.addObject("isUserCreated", true);
                mav.addObject("isUserNotCreated", false);

                mav.addObject("user", user);
            }
        }

        return new ModelAndView("redirect:topicsList.action");
    }

}
