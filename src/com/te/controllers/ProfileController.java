package com.te.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.te.beans.User;
import com.te.managers.UserManager;

@Controller
public class ProfileController {

    @Autowired
    private UserManager userManager;

    @RequestMapping(value = "/profile.action", method = RequestMethod.GET)
    public ModelAndView getProfile(HttpServletRequest request, @CookieValue(value = "bb_data", required = false) String hash) {
        ModelAndView mav = new ModelAndView("userProfile");
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:registration.action");
        }

        if (user != null) {
            mav.addObject("isAuthorized", true);
            mav.addObject("user", user);
        }
        return mav;
    }

    @RequestMapping(value = "/profile.action", method = RequestMethod.POST)
    public ModelAndView getProfilePost(@CookieValue(value = "bb_data", required = false) String hash,
            @RequestParam(value = "firstname", required = true) String firstName, @RequestParam(value = "lastname", required = true) String lastName,
            @RequestParam(value = "password", required = true) String password, @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "telephone", required = true) String telephone) {
        User user = userManager.getUser(hash);

        if (user == null) {
            return new ModelAndView("redirect:login.action");
        }

        userManager.changeProfile(firstName, lastName, password, email, telephone, user.getId());

        return new ModelAndView("redirect:profile.action");
    }

}